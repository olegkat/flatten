// Package flatten contains a simple function to flatten an array of
// arbitrarily nested arrays of integers into a flat array of
// integers, e.g.,[[1,2,[3]],4] -> [1,2,3,4].
package flatten

import "errors"

// ErrInvalidType is returned by Flatten when it encounters an array
// element of invalid type, i.e., anything other than a (nested) array
// or an integer.
var ErrInvalidType = errors.New("Invalid array element type")

// Flatten takes an array of arbitrarily nested arrays of integers
// (represented as arrays of the empty interface type) and returns a
// flattened representation of that array as an array of integers.
func Flatten(ar []interface{}) ([]int, error) {
	arOut := make([]int, 0, cap(ar))

	for _, e := range ar {
		switch v := e.(type) {
		case int:
			arOut = append(arOut, v)

		case []interface{}:
			arFlat, err := Flatten(v)
			if err != nil {
				return nil, err
			}
			arOut = append(arOut, arFlat...)

		default:
			return nil, ErrInvalidType
		}
	}

	return arOut, nil
}
