package flatten

import (
	"runtime/debug"
	"testing"
)

func cmpArrays(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}

	for i, v := range a {
		if v != b[i] {
			return false
		}
	}

	return true
}

func validateFlatten(t *testing.T, arTest []interface{}, arWant []int, errWant error) {
	arGot, errGot := Flatten(arTest)

	if errWant != errGot {
		t.Errorf("Want %v, got %v\n%s", errWant, errGot, string(debug.Stack()))
	}

	if true != cmpArrays(arWant, arGot) {
		t.Errorf("Want %v, got %v\n%s", arWant, arGot, string(debug.Stack()))
	}

}

func TestFlatten(t *testing.T) {
	validateFlatten(t,
		[]interface{}{},
		[]int{},
		nil)

	validateFlatten(t,
		[]interface{}{1},
		[]int{1},
		nil)

	validateFlatten(t,
		[]interface{}{1, 2, 3},
		[]int{1, 2, 3},
		nil)

	validateFlatten(t,
		[]interface{}{
			1, []interface{}{
				2, 3, 4,
			},
		},
		[]int{1, 2, 3, 4},
		nil)

	validateFlatten(t,
		[]interface{}{
			[]interface{}{
				1, 2,
				[]interface{}{3},
			},
			4,
		},
		[]int{1, 2, 3, 4},
		nil)

	validateFlatten(t,
		[]interface{}{
			[]interface{}{
				1, 2,
				[]interface{}{3},
			},
			4.1,
		},
		nil,
		ErrInvalidType)

	validateFlatten(t,
		[]interface{}{
			[]interface{}{
				1, 2,
				[]interface{}{3.},
			},
			4,
		},
		nil,
		ErrInvalidType)
}
